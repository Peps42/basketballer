import { createBrowserRouter, redirect, RouterProvider } from 'react-router-dom';
import { RoutePaths } from './routes/routePaths';
import { HomePage } from './pages/Home.page';
import { FiltersPage } from './pages/Filters.page';
import { InflationNationPage } from './pages/InflationNation.page';
import { PriceSpyPage } from './pages/PriceSpy.page';
import { RecipesPage } from './pages/Recipes.page';
import { StoresNearMePage } from './pages/StoresNearMe.page';
import { SettingsPage } from './pages/Settings.page';
import { TermsAndConditionsPage } from './pages/TermsAndConditions.page';
import { PrivacyPolicyPage } from './pages/PrivacyPolicy.page';
import { LogupPage } from './pages/Logup.page';
import { LoginPage } from './pages/Login.page';
import { ForgotPasswordPage } from './pages/ForgotPassword.page';

const router = createBrowserRouter([
  {
    path: RoutePaths.ROOT,
    element: <HomePage />,
    loader: () => redirect(RoutePaths.PRICE_SPY),
  },
  {
    path: RoutePaths.FILTERS,
    element: <FiltersPage />,
  },
  {
    path: RoutePaths.INFLATION_NATION,
    element: <InflationNationPage />,
  },
  {
    path: RoutePaths.PRICE_SPY,
    element: <PriceSpyPage />,
  },
  {
    path: RoutePaths.RECIPES,
    element: <RecipesPage />,
  },
  {
    path: RoutePaths.STORES_NEAR_ME,
    element: <StoresNearMePage />,
  },
  {
    path: RoutePaths.SETTINGS,
    element: <SettingsPage />,
  },
  {
    path: RoutePaths.TERMS_AND_CONDITIONS,
    element: <TermsAndConditionsPage />,
  },
  {
    path: RoutePaths.PRIVACY_POLICY,
    element: <PrivacyPolicyPage />,
  },
  {
    path: RoutePaths.LOGUP,
    element: <LogupPage />,
  },
  {
    path: RoutePaths.LOGIN,
    element: <LoginPage />,
  },
  {
    path: RoutePaths.FORGOT_PASSWORD,
    element: <ForgotPasswordPage />,
  },
]);

export function Router() {
  return <RouterProvider router={router} />;
}
