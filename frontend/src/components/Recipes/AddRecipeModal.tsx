import { useDisclosure } from '@mantine/hooks';
import { IconPlus } from '@tabler/icons-react';
import { Affix, Modal, rem, Button } from '@mantine/core';
import { useBoundStore } from '@/stores/store';
import { useTranslation } from 'react-i18next';

export function AddRecipeModal() {
  const [opened, { open, close }] = useDisclosure(false);
  const hamburgerOpen = useBoundStore((state) => state.hamburgerOpen);
  const { t } = useTranslation(['recipes']);
  const addRecipeT = t('add-recipe', { ns: 'recipes' });

  return (
    <>
      <Modal
        opened={opened}
        onClose={close}
        title={addRecipeT}
        fullScreen
        radius={0}
        transitionProps={{ transition: 'fade', duration: 200 }}
      >
        {/* TODO Modal content */}
      </Modal>

      <Affix position={{ bottom: 20, right: 20 }} hidden={opened || hamburgerOpen}>
        <Button
          onClick={open}
          leftSection={<IconPlus style={{ width: rem(18), height: rem(18) }} />}
        >
          {addRecipeT}
        </Button>
      </Affix>
    </>
  );
}
