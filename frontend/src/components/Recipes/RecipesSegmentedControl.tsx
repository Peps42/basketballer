import { SegmentedControl, Center, rem } from '@mantine/core';
import classes from './RecipesSegmentedControl.module.css';
import { useSearchParams } from 'react-router-dom';
import { useMediaQuery } from '@mantine/hooks';
import { IconUsers, IconUser, IconDownload, IconStar, IconThumbUp } from '@tabler/icons-react';
import { useTranslation } from 'react-i18next';

export function RecipesSegmentedControl() {
  const { t } = useTranslation(['common', 'recipes']);
  const largerScreen = useMediaQuery('(min-width: 56.25em)');
  const [searchParams, setSearchParams] = useSearchParams();
  const value = searchParams.get('type') ?? 'all';

  const setSearchParamsForSelectedType = (value: string) => setSearchParams({ type: value });

  return (
    <SegmentedControl
      radius={largerScreen ? 'xl' : 'md'}
      size="md"
      data={[
        {
          label: (
            <Center style={{ gap: 10 }}>
              <IconUsers style={{ width: rem(16), height: rem(16) }} />
              <span>{t('all', { ns: 'common' })}</span>
            </Center>
          ),
          value: 'all',
        },
        {
          label: (
            <Center style={{ gap: 10 }}>
              <IconUser style={{ width: rem(16), height: rem(16) }} />
              <span>{t('my-recipes', { ns: 'recipes' })}</span>
            </Center>
          ),
          value: 'self',
        },
        {
          label: (
            <Center style={{ gap: 10 }}>
              <IconThumbUp style={{ width: rem(16), height: rem(16) }} />
              <span>{t('liked', { ns: 'common' })}</span>
            </Center>
          ),
          value: 'liked',
        },
        {
          label: (
            <Center style={{ gap: 10 }}>
              <IconStar style={{ width: rem(16), height: rem(16) }} />
              <span>{t('favourites', { ns: 'common' })}</span>
            </Center>
          ),
          value: 'favourites',
        },
        {
          label: (
            <Center style={{ gap: 10 }}>
              <IconDownload style={{ width: rem(16), height: rem(16) }} />
              <span>{t('saved', { ns: 'common' })}</span>
            </Center>
          ),
          value: 'saved',
        },
      ]}
      classNames={classes}
      value={value}
      onChange={setSearchParamsForSelectedType}
      style={!largerScreen ? { width: '72%' } : {}}
      orientation={largerScreen ? 'horizontal' : 'vertical'}
    />
  );
}
