import { TextInput, ActionIcon, useMantineTheme, rem } from '@mantine/core';
import { IconSearch, IconArrowRight } from '@tabler/icons-react';
import { useMediaQuery } from '@mantine/hooks';
import { useTranslation } from 'react-i18next';

export function RecipesSearchBar() {
  const largerScreen = useMediaQuery('(min-width: 56.25em)');
  const theme = useMantineTheme();
  const { t } = useTranslation(['recipes']);

  return (
    <TextInput
      radius="xl"
      size="md"
      placeholder={t('search-recipes', { ns: 'recipes' })}
      rightSectionWidth={42}
      leftSection={<IconSearch style={{ width: rem(18), height: rem(18) }} stroke={1.5} />}
      rightSection={
        <ActionIcon size={32} radius="xl" color={theme.primaryColor} variant="filled">
          <IconArrowRight style={{ width: rem(18), height: rem(18) }} stroke={1.5} />
        </ActionIcon>
      }
      style={!largerScreen ? { width: '72%' } : { width: '536px' }}
    />
  );
}
