import { rem } from '@mantine/core';

interface SuperValueIconProps extends React.ComponentPropsWithoutRef<'svg'> {
  size?: number | string;
}

export function SuperValueIcon({ size, style, ...others }: SuperValueIconProps) {
  return (
    <svg
      version="1.0"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 16.000000 16.000000"
      style={{ width: rem(size), height: rem(size), ...style }}
      {...others}
    >
      <g
        transform="translate(0.000000,16.000000) scale(0.100000,-0.100000)"
        fill="#D30C00"
        stroke="none"
      >
        <path d="M100 106 c-31 -54 -35 -57 -43 -38 -12 27 -43 31 -32 4 4 -9 12 -30 17 -45 16 -44 39 -29 70 47 37 91 30 107 -12 32z" />
      </g>
    </svg>
  );
}
