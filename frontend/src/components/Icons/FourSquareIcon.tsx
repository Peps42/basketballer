import { rem } from '@mantine/core';

interface FourSquareIconProps extends React.ComponentPropsWithoutRef<'svg'> {
  size?: number | string;
}

export function FourSquareIcon({ size, style, ...others }: FourSquareIconProps) {
  return (
    <svg
      viewBox="0 0 40 41"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      style={{ width: rem(size), height: rem(size), ...style }}
      {...others}
    >
      <g
        id="Styles,-Icons,-Logos-and-Controls"
        stroke="none"
        stroke-width="1"
        fill="none"
        fill-rule="evenodd"
      >
        <g id="Logos-and-charlie" transform="translate(-123.000000, -204.000000)">
          <g id="4-SQUARE-LOGO-SMALL" transform="translate(123.000000, 204.000000)">
            <rect id="Rectangle" fill="#278342" x="0" y="0" width="39.89" height="41"></rect>
            <path
              d="M0,0 L0,41 L39.89,41 L39.89,0 L0,0 Z M38.32,39.44 L1.56,39.44 L1.56,1.56 L38.32,1.56 L38.32,39.44 Z"
              id="Shape"
              fill="#FFD600"
            ></path>
            <path
              d="M17.08,34.58 L17.08,29.68 L8,29.68 L4.39,23.48 L17.09,6.48 L30.16,6.48 L30.16,22.16 L33.42,22.16 L33.42,29.68 L30.16,29.68 L30.16,34.58 L17.08,34.58 Z M17.08,22.16 L17.08,17.45 L13.59,22.15 L17.08,22.16 Z"
              id="Shape"
              fill="#FFD600"
            ></path>
            <path
              d="M28.83,23.48 L28.83,7.82 L17.75,7.82 L6,23.57 L8.78,28.35 L18.44,28.35 L18.44,33.24 L28.83,33.24 L28.83,28.35 L32.09,28.35 L32.09,23.48 L28.83,23.48 Z M18.41,23.48 L10.94,23.48 L18.41,13.42 L18.41,23.48 Z"
              id="Shape"
              fill="#ED1D24"
            ></path>
          </g>
        </g>
      </g>
    </svg>
  );
}
