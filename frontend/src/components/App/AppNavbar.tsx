import {
  IconBellRinging,
  IconShoppingCartUp,
  IconSettings,
  IconLogout,
  IconLogin,
  IconSpy,
  IconFilterCog,
  IconList,
  IconShoppingCart,
  IconBuildingStore,
  IconChefHat,
  IconReceipt,
  IconUserUp,
} from '@tabler/icons-react';
import classes from './AppNavbar.module.css';
import { useTranslation } from 'react-i18next';
import { RoutePaths } from '@/routes/routePaths';

export function AppNavbar() {
  const { t } = useTranslation('common');
  const pathname = window.location.pathname;
  const data = [
    { link: RoutePaths.FILTERS, label: t('filters', { ns: 'common' }), icon: IconFilterCog },
    {
      link: RoutePaths.INFLATION_NATION,
      label: t('inflation-nation', { ns: 'common' }),
      icon: IconShoppingCartUp,
    },
    {
      link: RoutePaths.NOTIFICATIONS,
      label: t('notifications', { ns: 'common' }),
      icon: IconBellRinging,
    },
    { link: RoutePaths.PRICE_SPY, label: t('price-spy', { ns: 'common' }), icon: IconSpy },
    { link: RoutePaths.RECEIPTS, label: t('receipts', { ns: 'common' }), icon: IconReceipt },
    { link: RoutePaths.RECIPES, label: t('recipes', { ns: 'common' }), icon: IconChefHat },
    {
      link: RoutePaths.SHOPPING_LISTS,
      label: t('shopping-lists', { ns: 'common' }),
      icon: IconList,
    },
    {
      link: RoutePaths.SHOPPING_PLANS,
      label: t('shopping-plans', { ns: 'common' }),
      icon: IconShoppingCart,
    },
    {
      link: RoutePaths.STORES_NEAR_ME,
      label: t('stores-near-me', { ns: 'common' }),
      icon: IconBuildingStore,
    },
  ];

  const links = data.map((item) => (
    <a
      className={classes.link}
      data-active={item.link === pathname || undefined}
      href={item.link}
      key={item.label}
    >
      <item.icon className={classes.linkIcon} stroke={1.5} />
      <span>{item.label}</span>
    </a>
  ));

  const logout = () => {
    // TODO
    // make logout call
    // clear auth cookie
  };

  return (
    <nav className={classes.navbar}>
      <div className={classes.navbarMain}>{links}</div>

      <div className={classes.footer}>
        <a href={RoutePaths.SETTINGS} className={classes.link}>
          <IconSettings className={classes.linkIcon} stroke={1.5} />
          <span>{t('settings', { ns: 'common' })}</span>
        </a>

        <a href={RoutePaths.PRICE_SPY} className={classes.link} onClick={logout}>
          <IconLogout className={classes.linkIcon} stroke={1.5} />
          <span>{t('logout', { ns: 'common' })}</span>
        </a>

        <a href={RoutePaths.LOGUP} className={classes.link}>
          <IconUserUp className={classes.linkIcon} stroke={1.5} />
          <span>{t('logup', { ns: 'common' })}</span>
        </a>

        <a href={RoutePaths.LOGIN} className={classes.link}>
          <IconLogin className={classes.linkIcon} stroke={1.5} />
          <span>{t('login', { ns: 'common' })}</span>
        </a>
      </div>
    </nav>
  );
}
