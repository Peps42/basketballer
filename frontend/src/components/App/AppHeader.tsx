import { useBoundStore } from '@/stores/store';
import { Group, Burger, Text } from '@mantine/core';
import { BasketBallerIcon } from '../Icons/BasketBallerIcon';
import { ColorSchemeToggle } from '../ColorSchemeToggle/ColorSchemeToggle';

export function AppHeader() {
  const hamburgerOpen = useBoundStore((state) => state.hamburgerOpen);
  const toggleHamburgerOpen = useBoundStore((state) => state.toggleHamburgerOpen);

  return (
    <Group h="100%" px="xl" justify="space-between">
      <Group>
        <Burger opened={hamburgerOpen} onClick={toggleHamburgerOpen} hiddenFrom="sm" size="sm" />
        <BasketBallerIcon size={32} />
        <Text size="xl" fw={900}>
          BasketBaller
        </Text>
      </Group>
      <ColorSchemeToggle />
    </Group>
  );
}
