import { Text, Group, ActionIcon, rem } from '@mantine/core';
import { IconPlus, IconMinus } from '@tabler/icons-react';
import classes from './CircleRadiusPicker.module.css';
import { useBoundStore } from '@/stores/store';
import { useTranslation } from 'react-i18next';

const maxCircleRadiusM = 20000;
const minCircleRadiusM = 1000;

export function CircleRadiusPicker() {
  const { t } = useTranslation(['stores-near-me']);
  const circleRadiusM = useBoundStore((state) => state.circleRadiusM);
  const setCirlceRadiusM = useBoundStore((state) => state.setCircleRadiusM);

  const incrementCircleRadius = () => {
    if (circleRadiusM === maxCircleRadiusM) return;
    setCirlceRadiusM(circleRadiusM + 1000);
  };
  const decrementCircleRadius = () => {
    if (circleRadiusM === minCircleRadiusM) return;
    setCirlceRadiusM(circleRadiusM - 1000);
  };

  return (
    <Group justify="flex-end" my={10}>
      <ActionIcon size={32} onClick={decrementCircleRadius}>
        <IconMinus style={{ width: rem(18), height: rem(18) }} stroke={1.5} />
      </ActionIcon>
      <Text className={classes.text}>
        {t('within-km', { ns: 'stores-near-me', distance: circleRadiusM / 1000 })}
      </Text>
      <ActionIcon size={32} onClick={incrementCircleRadius}>
        <IconPlus style={{ width: rem(18), height: rem(18) }} stroke={1.5} />
      </ActionIcon>
    </Group>
  );
}
