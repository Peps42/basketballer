import { useMapEvents } from 'react-leaflet';
import { useBoundStore } from '@/stores/store';

export function MapEvents() {
  const setMapZoom = useBoundStore((state) => state.setMapZoom);

  const mapEvents = useMapEvents({
    zoomend: () => {
      setMapZoom(mapEvents.getZoom());
    },
  });

  return <></>;
}
