import { ActionIcon, rem } from '@mantine/core';
import { IconFocus2 } from '@tabler/icons-react';
import { LatLngTuple, Map } from 'leaflet';

interface RecenterButtonProps {
  map: Map;
  coords: LatLngTuple;
}

export function RecenterButton({ map, coords }: RecenterButtonProps) {
  const defaultZoom = 14;

  return (
    <>
      <ActionIcon size={32} onClick={() => map.setView(coords, defaultZoom)}>
        <IconFocus2 style={{ width: rem(18), height: rem(18) }} stroke={1.5} />
      </ActionIcon>
    </>
  );
}
