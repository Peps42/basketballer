import { CloseButton } from '@mantine/core';
import { logos } from './SuperMarketLogos';
import classes from './SuperMarketPill.module.css';
import { supermarkets } from './supermarkets';

interface SupermarketPillProps extends React.ComponentPropsWithoutRef<'div'> {
  value: string;
  onRemove?(): void;
}

export function SuperMarketPill({ value, onRemove, ...others }: SupermarketPillProps) {
  const OptionLogo = logos[value.slice(0, 2).toUpperCase()];
  const supermarket = supermarkets.find((item) => item.value === value);

  return (
    <div className={classes.pill} {...others}>
      <div className={classes.logo}>
        <OptionLogo />
      </div>
      <div className={classes.label}>{supermarket?.label}</div>
      <CloseButton
        onMouseDown={onRemove}
        variant="transparent"
        color="gray"
        size={22}
        iconSize={14}
        tabIndex={-1}
      />
    </div>
  );
}
