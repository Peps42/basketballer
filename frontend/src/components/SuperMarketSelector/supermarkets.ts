export const supermarkets = [
  { value: 'nw', label: 'New World' },
  { value: 'nw-wanaka', label: 'New World Wanaka' },
  { value: 'ww', label: 'Woolworths' },
  { value: 'ww-alexandra', label: 'Countdown Alexandra' },
  { value: 'ps', label: "Pak'nSave" },
  { value: 'fc', label: 'FreshChoice' },
  { value: 'sv', label: 'SuperValue' },
  { value: 'fs', label: 'Four Square' },
];
