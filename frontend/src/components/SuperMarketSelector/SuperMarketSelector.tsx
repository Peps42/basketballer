import {
  PillsInput,
  Pill,
  Combobox,
  CheckIcon,
  Group,
  useCombobox,
  ScrollArea,
} from '@mantine/core';
import { logos } from './SuperMarketLogos';
import { useState } from 'react';
import { SuperMarketPill } from './SuperMarketPill';
import { supermarkets } from './supermarkets';
import { useTranslation } from 'react-i18next';

export function SuperMarketSelector() {
  const { t } = useTranslation(['common']);

  const combobox = useCombobox({
    onDropdownClose: () => combobox.resetSelectedOption(),
    onDropdownOpen: () => combobox.updateSelectedOptionIndex('active'),
  });

  const [search, setSearch] = useState('');
  const [value, setValue] = useState<string[]>([]);

  const handleValueSelect = (val: string) =>
    setValue((current) =>
      current.includes(val) ? current.filter((v) => v !== val) : [...current, val]
    );

  const handleValueRemove = (val: string) =>
    setValue((current) => current.filter((v) => v !== val));

  const values = value.map((item) => (
    <SuperMarketPill key={item} value={item} onRemove={() => handleValueRemove(item)} />
  ));

  const options = supermarkets
    .filter((item) => item.label.toLowerCase().includes(search.trim().toLowerCase()))
    .map((item) => {
      const OptionLogo = logos[item.value.slice(0, 2).toUpperCase()];
      return (
        <Combobox.Option value={item.value} key={item.value} active={value.includes(item.value)}>
          <Group justify="space-between">
            <Group gap={7}>
              <OptionLogo />
              <span>{item.label}</span>
            </Group>
            {value.includes(item.value) ? <CheckIcon size={12} /> : null}
          </Group>
        </Combobox.Option>
      );
    });

  return (
    <Combobox store={combobox} onOptionSubmit={handleValueSelect} withinPortal={false}>
      <Combobox.DropdownTarget>
        <PillsInput
          pointer
          onClick={() => combobox.toggleDropdown()}
          label={t('supermarkets', { ns: 'common' })}
        >
          <Pill.Group>
            {values}

            <Combobox.EventsTarget>
              <PillsInput.Field
                onBlur={() => combobox.closeDropdown()}
                value={search}
                placeholder={values.length === 0 ? t('pick-supermarkets', { ns: 'common' }) : ''}
                onChange={(event) => {
                  combobox.updateSelectedOptionIndex();
                  setSearch(event.currentTarget.value);
                }}
                onKeyDown={(event) => {
                  if (event.key === 'Backspace' && search.length === 0) {
                    event.preventDefault();
                    handleValueRemove(value[value.length - 1]);
                  }
                }}
              />
            </Combobox.EventsTarget>
          </Pill.Group>
        </PillsInput>
      </Combobox.DropdownTarget>

      <Combobox.Dropdown>
        <Combobox.Options>
          <ScrollArea.Autosize mah={200} type="scroll">
            {options.length > 0 ? (
              options
            ) : (
              <Combobox.Empty>{t('no-supermarkets-found', { ns: 'common' })}</Combobox.Empty>
            )}
          </ScrollArea.Autosize>
        </Combobox.Options>
      </Combobox.Dropdown>
    </Combobox>
  );
}
