import { NewWorldIcon } from '../Icons/NewWorldIcon';
import { WoolworthsIcon } from '../Icons/WoolworthsIcon';
import { PakNSaveIcon } from '../Icons/PakNSaveIcon';
import { FreshChoiceIcon } from '../Icons/FreshChoiceIcon';
import { SuperValueIcon } from '../Icons/SuperValueIcon';
import { FourSquareIcon } from '../Icons/FourSquareIcon';

const logoProps = {
  size: 14,
  style: { display: 'block' },
};

function NewWorldLogo() {
  return <NewWorldIcon {...logoProps} />;
}

function WoolworthsLogo() {
  return <WoolworthsIcon {...logoProps} />;
}

function PakNSaveLogo() {
  return <PakNSaveIcon {...logoProps} />;
}

function FreshChoiceLogo() {
  return <FreshChoiceIcon {...logoProps} />;
}

function SuperValueLogo() {
  return <SuperValueIcon {...logoProps} />;
}

function FourSquareLogo() {
  return <FourSquareIcon {...logoProps} />;
}

export const logos: Record<string, React.FC> = {
  NW: NewWorldLogo,
  WW: WoolworthsLogo,
  PS: PakNSaveLogo,
  FC: FreshChoiceLogo,
  SV: SuperValueLogo,
  FS: FourSquareLogo,
};
