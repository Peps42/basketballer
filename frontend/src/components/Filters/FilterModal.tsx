import { rem, Group, TextInput, Button, Text } from '@mantine/core';
import { IconEdit, IconSpy, IconTrash } from '@tabler/icons-react';
import { SuperMarketSelector } from '../SuperMarketSelector/SuperMarketSelector';
import { useTranslation } from 'react-i18next';

interface Filter {
  id: number;
  name: string;
  lastUpdated: Date;
}

interface FilterModalProps {
  filter: Filter;
}

export function FilterModal({ filter }: FilterModalProps) {
  const { t } = useTranslation();
  // TODO
  const editFilter = () => {};
  // TODO
  const searchByFilter = () => {};
  // TODO
  const deleteFilter = () => {};

  return (
    <>
      <TextInput label={t('filter-name', { ns: 'common' })} disabled required />
      <SuperMarketSelector />
      <Text mt={4} fs="italic" ta="right">
        {t('last-updated', { ns: 'common', lastUpdated: filter.lastUpdated.toLocaleDateString() })}
      </Text>
      <Group justify="flex-end" gap={5} mt={10}>
        <Button
          leftSection={<IconEdit style={{ width: rem(18), height: rem(18) }} stroke={2} />}
          color="indigo"
          onClick={editFilter}
        >
          {t('edit', { ns: 'common' })}
        </Button>
        <Button
          leftSection={<IconSpy style={{ width: rem(18), height: rem(18) }} stroke={2} />}
          onClick={searchByFilter}
        >
          {t('search', { ns: 'common' })}
        </Button>
        <Button
          leftSection={<IconTrash style={{ width: rem(18), height: rem(18) }} stroke={2} />}
          color="red"
          onClick={deleteFilter}
        >
          {t('delete', { ns: 'common' })}
        </Button>
      </Group>
    </>
  );

  // TODO
  // product categories
  // product
  // ingredients
  // price
  // allergens
  // nutrional info
  // size
}
