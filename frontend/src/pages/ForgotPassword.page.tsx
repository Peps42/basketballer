import {
  Paper,
  Title,
  Text,
  TextInput,
  Button,
  Container,
  Group,
  Anchor,
  Center,
  rem,
} from '@mantine/core';
import { IconArrowLeft } from '@tabler/icons-react';
import classes from './ForgotPassword.module.css';
import { useTranslation } from 'react-i18next';
import { RoutePaths } from '@/routes/routePaths';

export function ForgotPasswordPage() {
  const { t } = useTranslation(['common', 'user']);

  return (
    <Container size={460} mt="16%">
      <Title className={classes.title} ta="center">
        {t('forgot-your-password', { ns: 'user' })}
      </Title>
      <Text c="dimmed" fz="sm" ta="center">
        {t('enter-your-email', { ns: 'user' })}
      </Text>

      <Paper withBorder shadow="md" p={30} radius="md" mt="xl">
        <TextInput
          label={t('email', { ns: 'common' })}
          placeholder={t('email-placeholder', { ns: 'common' })}
          required
        />
        <Group justify="space-between" mt="lg" className={classes.controls}>
          <Anchor c="dimmed" size="sm" className={classes.control} href={RoutePaths.LOGIN}>
            <Center inline>
              <IconArrowLeft style={{ width: rem(12), height: rem(12) }} stroke={1.5} />
              {t('back-to-login', { ns: 'user' })}
            </Center>
          </Anchor>
          <Button className={classes.control}>{t('reset-password', { ns: 'user' })}</Button>
        </Group>
      </Paper>
    </Container>
  );
}
