import { Container } from '@mantine/core';
import { DataTable } from 'mantine-datatable';
import { modals } from '@mantine/modals';
import classes from './Filters.module.css';
import { FilterModal } from '@/components/Filters/FilterModal';
import { useTranslation } from 'react-i18next';

const now = new Date();
const filters = [
  { id: 1, name: 'Eggs', lastUpdated: now },
  { id: 2, name: 'Bread', lastUpdated: now },
  { id: 3, name: 'Chicken', lastUpdated: now },
  { id: 4, name: 'Peach', lastUpdated: now },
  { id: 5, name: 'Tofu', lastUpdated: now },
];

export function FiltersPage() {
  const { t } = useTranslation();

  return (
    <Container my={40} className={classes.text}>
      <DataTable
        striped
        highlightOnHover
        textSelectionDisabled
        columns={[
          {
            accessor: 'name',
            title: t('filter-name', { ns: 'common' }),
          },
        ]}
        records={filters}
        onRowClick={({ record }) => {
          modals.open({
            title: record.name,
            children: <FilterModal filter={record} />,
          });
        }}
      ></DataTable>
    </Container>
  );
}
