import { LineChart, PieChart } from '@mantine/charts';
import classes from './InflationNation.module.css';
import { Grid, Text } from '@mantine/core';
import {
  mockInflationData,
  mockWeeklyExpensesData,
  mockFoodGroupExpensesData,
} from '@/api/mock-data';
import { useTranslation } from 'react-i18next';

export function InflationNationPage() {
  const { t } = useTranslation(['inflation-nation']);

  return (
    <Grid>
      <Grid.Col span={{ base: 12, xs: 4 }}>
        <Text fz="lg" fw={500} className={classes.graphTitle} mt="md">
          {t('inflation-indicators', { ns: 'inflation-nation' })}
        </Text>
        <LineChart
          h={300}
          data={mockInflationData}
          dataKey="date"
          series={[
            {
              name: 'cpi',
              label: t('consumer-price-index', { ns: 'inflation-nation' }),
              color: 'var(--cpi-line-color)',
            },
            {
              name: 'fpi',
              label: t('food-price-index', { ns: 'inflation-nation' }),
              color: 'var(--fpi-line-color)',
            },
          ]}
          withLegend
          curveType="linear"
          connectNulls
          className={classes.root}
          unit="%"
        />
      </Grid.Col>
      <Grid.Col span={{ base: 12, xs: 4 }}>
        <Text fz="lg" fw={500} className={classes.graphTitle} mt="md">
          {t('weekly-shopping-expenses', { ns: 'inflation-nation' })}
        </Text>
        <LineChart
          h={300}
          data={mockWeeklyExpensesData}
          dataKey="date"
          series={[
            {
              name: 'expenses',
              label: t('expenses', { ns: 'inflation-nation' }),
              color: 'var(--expenses-line-color)',
            },
          ]}
          withLegend
          curveType="linear"
          connectNulls
          className={classes.root}
        />
      </Grid.Col>
      <Grid.Col span={{ base: 12, xs: 4 }}>
        <Text fz="lg" fw={500} className={classes.graphTitle} mt="md">
          {t('shopping-expenses-by-subgroups', { ns: 'inflation-nation' })}
        </Text>
        <PieChart
          data={mockFoodGroupExpensesData}
          withTooltip
          tooltipDataSource="segment"
          mx="auto"
        />
      </Grid.Col>
    </Grid>
  );
}
