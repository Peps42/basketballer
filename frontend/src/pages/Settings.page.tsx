import { Card, Text, SimpleGrid, UnstyledButton, Center } from '@mantine/core';
import {
  IconUser,
  IconAppWindow,
  IconLock,
  IconMessageQuestion,
  IconNotes,
  IconMessage,
  IconChecklist,
  IconLicense,
  IconInfoCircle,
} from '@tabler/icons-react';
import classes from './Settings.module.css';
import { useTranslation } from 'react-i18next';
import { RoutePaths } from '@/routes/routePaths';

export function SettingsPage() {
  const { t } = useTranslation(['common']);

  const settings = [
    {
      title: t('personal-settings', { ns: 'common' }),
      icon: IconUser,
      link: RoutePaths.PERSONAL_SETTINGS,
    },
    {
      title: t('app-settings', { ns: 'common' }),
      icon: IconAppWindow,
      link: RoutePaths.APP_SETTINGS,
    },
    { title: t('security', { ns: 'common' }), icon: IconLock, link: RoutePaths.SECURITY },
    { title: t('faq', { ns: 'common' }), icon: IconMessageQuestion, link: RoutePaths.FAQ },
    { title: t('contact-us', { ns: 'common' }), icon: IconMessage, link: RoutePaths.CONTACT_US },
    {
      title: t('release-notes', { ns: 'common' }),
      icon: IconNotes,
      link: RoutePaths.RELEASE_NOTES,
    },
    {
      title: t('terms-and-conditions', { ns: 'common' }),
      icon: IconChecklist,
      link: RoutePaths.TERMS_AND_CONDITIONS,
    },
    {
      title: t('privacy-policy', { ns: 'common' }),
      icon: IconLicense,
      link: RoutePaths.PRIVACY_POLICY,
    },
    { title: t('about', { ns: 'common' }), icon: IconInfoCircle, link: RoutePaths.ABOUT },
  ];

  const items = settings.map((item) => (
    <UnstyledButton key={item.title} className={classes.item} component="a" href={item.link}>
      <item.icon size="2rem" className={classes.icon} />
      <Text size="xs" mt={7}>
        {item.title}
      </Text>
    </UnstyledButton>
  ));

  return (
    <Center style={{ height: '100%' }}>
      <Card withBorder radius="md" className={classes.card}>
        <Text className={classes.title}>{t('settings', { ns: 'common' })}</Text>
        <SimpleGrid cols={3} mt="md">
          {items}
        </SimpleGrid>
      </Card>
    </Center>
  );
}

// TODO
// personal settings
// - username
// - email
// app settings
// - push notifications
// - email notifications
// - recipe notifications
// - language
// security
// - change password
