import { AddRecipeModal } from '@/components/Recipes/AddRecipeModal';
import { RecipesSearchBar } from '@/components/Recipes/RecipesSearchBar';
import { RecipesSegmentedControl } from '@/components/Recipes/RecipesSegmentedControl';
import { Center, Container } from '@mantine/core';

export function RecipesPage() {
  return (
    <Container>
      <Center my={40}>
        <RecipesSegmentedControl />
      </Center>
      <Center>
        <RecipesSearchBar />
      </Center>
      <AddRecipeModal />
    </Container>
    // <Recipes />
  );
}
