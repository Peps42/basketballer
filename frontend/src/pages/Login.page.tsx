import {
  TextInput,
  PasswordInput,
  Checkbox,
  Anchor,
  Paper,
  Title,
  Text,
  Container,
  Group,
  Button,
} from '@mantine/core';
import classes from './Login.module.css';
import { useTranslation } from 'react-i18next';
import { RoutePaths } from '@/routes/routePaths';

export function LoginPage() {
  const { t } = useTranslation(['common', 'user']);

  return (
    <Container size={420} mt="12%">
      <Title ta="center" className={classes.title}>
        {t('welcome-back', { ns: 'user' })}
      </Title>
      <Text c="dimmed" size="sm" ta="center" mt={5}>
        {t('do-not-have-account', { ns: 'user' })}
        <Anchor size="sm" href={RoutePaths.LOGUP}>
          {t('create-account', { ns: 'user' })}
        </Anchor>
      </Text>

      <Paper withBorder shadow="md" p={30} mt={30} radius="md">
        <TextInput
          label={t('email', { ns: 'common' })}
          placeholder={t('email-placeholder', { ns: 'common' })}
          required
        />
        <PasswordInput
          label={t('password', { ns: 'common' })}
          placeholder={t('password-placeholder', { ns: 'common' })}
          required
          mt="md"
        />
        <Group justify="space-between" mt="lg">
          <Checkbox label={t('remember-me', { ns: 'user' })} />
          <Anchor size="sm" href={RoutePaths.FORGOT_PASSWORD}>
            {t('forgot-password', { ns: 'user' })}
          </Anchor>
        </Group>
        <Button fullWidth mt="xl">
          {t('login', { ns: 'common' })}
        </Button>
      </Paper>
    </Container>
  );
}
