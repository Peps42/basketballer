import {
  TextInput,
  PasswordInput,
  Checkbox,
  Anchor,
  Paper,
  Title,
  Text,
  Container,
  Group,
  Button,
} from '@mantine/core';
import classes from './Login.module.css';
import { Trans, useTranslation } from 'react-i18next';
import { RoutePaths } from '@/routes/routePaths';

export function LogupPage() {
  const { t } = useTranslation(['common', 'user']);

  return (
    <Container size={420} mt="8%">
      <Title ta="center" className={classes.title}>
        {t('create-your-account', { ns: 'user' })}
      </Title>
      <Text c="dimmed" size="sm" ta="center" mt={5}>
        {t('already-have-an-account', { ns: 'user' })}
        <Anchor size="sm" href={RoutePaths.LOGIN}>
          {t('login', { ns: 'common' })}
        </Anchor>
      </Text>

      <Paper withBorder shadow="md" p={30} mt={30} radius="md">
        <TextInput
          label={t('username', { ns: 'common' })}
          placeholder={t('username-placeholder', { ns: 'common' })}
          description={t('username-description', { ns: 'common' })}
          required
        />
        <TextInput
          label={t('email', { ns: 'common' })}
          placeholder={t('email-placeholder', { ns: 'common' })}
          required
          mt="md"
        />
        <PasswordInput
          label={t('password', { ns: 'common' })}
          placeholder={t('password-placeholder', { ns: 'common' })}
          required
          mt="md"
        />
        <PasswordInput
          label={t('confirm-password', { ns: 'common' })}
          placeholder={t('password-placeholder', { ns: 'common' })}
          required
          mt="md"
        />
        <Group justify="space-between" mt="lg">
          <Checkbox
            label={
              <Trans
                i18nKey="user:i-accept"
                components={[
                  <Anchor href={RoutePaths.TERMS_AND_CONDITIONS} target="_blank" inherit></Anchor>,
                  <Anchor href={RoutePaths.PRIVACY_POLICY} target="_blank" inherit></Anchor>,
                ]}
              />
            }
          />
        </Group>
        <Button fullWidth mt="xl">
          {t('logup', { ns: 'common' })}
        </Button>
      </Paper>
    </Container>
  );
}
