import { LatLngTuple } from 'leaflet';
import { MapContainer, TileLayer, Circle } from 'react-leaflet';
import { useBoundStore } from '@/stores/store';
import { useGeolocated } from 'react-geolocated';
import { CircleRadiusPicker } from '@/components/StoresNearMe/CircleRadiusPicker';
import { MapEvents } from '@/components/StoresNearMe/MapEvents';
import { Group } from '@mantine/core';
import { useRef } from 'react';
import { RecenterButton } from '@/components/StoresNearMe/RecenterButton';
import { Map } from 'leaflet';

export function StoresNearMePage() {
  const mapRef = useRef<Map>(null);
  const hamburgerOpen = useBoundStore((state) => state.hamburgerOpen);
  const circleRadiusM = useBoundStore((state) => state.circleRadiusM);
  const mapZoom = useBoundStore((state) => state.mapZoom);
  const { coords } = useGeolocated({
    positionOptions: {
      enableHighAccuracy: true,
    },
    userDecisionTimeout: 10000,
  });
  const defaultCoords = [-43.532, 172.6306]; // Christchurch, New Zealand
  const startingCoords = (
    coords ? [coords.latitude, coords.longitude] : defaultCoords
  ) as LatLngTuple;

  if (hamburgerOpen) return;

  return (
    <>
      <MapContainer
        center={startingCoords}
        minZoom={5}
        zoom={mapZoom}
        maxZoom={16}
        scrollWheelZoom={true}
        style={{ height: '95%' }}
        maxBounds={[
          [-46.641235447, 166.509144322],
          [-34.4506617165, 178.517093541],
        ]}
        ref={mapRef}
      >
        <MapEvents />
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Circle center={startingCoords} pathOptions={{ color: 'green' }} radius={circleRadiusM} />
      </MapContainer>
      <Group justify="flex-end">
        <RecenterButton map={mapRef.current!} coords={startingCoords} />
        <CircleRadiusPicker />
      </Group>
    </>
  );
}
