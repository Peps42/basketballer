import '@mantine/core/styles.css';
import '@mantine/charts/styles.css';
import '@mantine/notifications/styles.css';
import 'mantine-datatable/styles.css';
import './i18n';
import { MantineProvider, AppShell, ScrollArea } from '@mantine/core';
import { Router } from './Router';
import { theme } from './theme';
import { AppNavbar } from './components/App/AppNavbar';
import { useBoundStore } from './stores/store';
import { AppHeader } from './components/App/AppHeader';
import { ModalsProvider } from '@mantine/modals';

export default function App() {
  const hamburgerOpen = useBoundStore((state) => state.hamburgerOpen);

  return (
    <MantineProvider theme={theme}>
      <ModalsProvider>
        <AppShell
          header={{ height: 60 }}
          navbar={{ width: 300, breakpoint: 'sm', collapsed: { mobile: !hamburgerOpen } }}
          padding="md"
        >
          <AppShell.Header>
            <AppHeader />
          </AppShell.Header>
          <AppShell.Navbar>
            <AppShell.Section component={ScrollArea} scrollbars="y">
              <AppNavbar />
            </AppShell.Section>
          </AppShell.Navbar>
          <AppShell.Main h="200px">
            <Router />
          </AppShell.Main>
        </AppShell>
      </ModalsProvider>
    </MantineProvider>
  );
}
