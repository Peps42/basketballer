export const mockInflationData = [
  {
    date: 'Dec-22',
    cpi: 7.2,
    fpi: 11.3,
  },
  {
    date: 'Mar-23',
    cpi: 6.7,
    fpi: 12.1,
  },
  {
    date: 'Jun-23',
    cpi: 6.0,
    fpi: 12.5,
  },
  {
    date: 'Sep-23',
    cpi: 5.6,
    fpi: null,
  },
  {
    date: 'Dec-23',
    cpi: 4.7,
    fpi: null,
  },
];

export const mockWeeklyExpensesData = [
  {
    date: '8-Jan-24',
    expenses: 122.47,
  },
  {
    date: '15-Jan-24',
    expenses: 109.24,
  },
  {
    date: '22-Jan-24',
    expenses: 69.87,
  },
  {
    date: '29-Jan-24',
    expenses: 123.21,
  },
  {
    date: '5-Feb-24',
    expenses: 180.76,
  },
];

export const mockFoodGroupExpensesData = [
  { name: 'Fruit & Vegetables', value: 12.49, color: 'green.6' },
  { name: 'Meat', value: 32.25, color: 'red.6' },
  { name: 'Dairy', value: 16.56, color: 'blue.6' },
  { name: 'Junk', value: 12.2, color: 'gray.6' },
];
