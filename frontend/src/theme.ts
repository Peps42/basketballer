import { createTheme } from '@mantine/core';

export const theme = createTheme({
  primaryColor: 'brand-green',
  colors: {
    'brand-green': [
      '#ebfbee',
      '#d3f9d8',
      '#b2f2bb',
      '#8ce99a',
      '#69db7c',
      '#51cf66',
      '#40c057',
      '#37b24d',
      '#2f9e44',
      '#2b8a3e',
    ],
  },
});
