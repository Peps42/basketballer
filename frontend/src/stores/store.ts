import { create } from 'zustand';
import { createPageSlice } from './page.slice';
import { devtools, persist } from 'zustand/middleware';
import { createStoresNearMeSlice } from './storeNearMe.slice';
import { CombinedState } from './types';

export const useBoundStore = create<CombinedState>()(
  devtools(
    persist(
      (...a) => ({
        ...createPageSlice(...a),
        ...createStoresNearMeSlice(...a),
      }),
      {
        name: 'basketBallerStore',
        partialize: (state) => ({
          hamburgerOpen: state.hamburgerOpen,
          circleRadiusM: state.circleRadiusM,
          mapZoom: state.mapZoom,
        }),
      }
    )
  )
);
