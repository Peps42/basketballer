import { StateCreator } from 'zustand';
import { PageSlice, CombinedState } from './types';

export const createPageSlice: StateCreator<
  CombinedState,
  [['zustand/persist', unknown]],
  [],
  PageSlice
> = (set) => ({
  hamburgerOpen: false,
  toggleHamburgerOpen: () => set((state) => ({ hamburgerOpen: !state.hamburgerOpen })),
});
