export interface PageSlice {
  hamburgerOpen: boolean;
  toggleHamburgerOpen: () => void;
}

export interface StoresNearMeSlice {
  circleRadiusM: number;
  setCircleRadiusM: (circleRadiusM: number) => void;
  mapZoom: number;
  setMapZoom: (mapZoom: number) => void;
}

export type CombinedState = PageSlice & StoresNearMeSlice;
