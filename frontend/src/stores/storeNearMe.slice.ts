import { StateCreator } from 'zustand';
import { StoresNearMeSlice, CombinedState } from './types';

export const createStoresNearMeSlice: StateCreator<
  CombinedState,
  [['zustand/persist', unknown]],
  [],
  StoresNearMeSlice
> = (set) => ({
  circleRadiusM: 3000,
  setCircleRadiusM: (circleRadiusM) => set(() => ({ circleRadiusM: circleRadiusM })),
  mapZoom: 14,
  setMapZoom: (mapZoom) => set(() => ({ mapZoom: mapZoom })),
});
